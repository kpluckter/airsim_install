AirSim UE4Editor Install Instructions for Ubuntu 16.04:

1. Install Unreal Engine and AirSim (used link below)
https://github.com/Microsoft/AirSim/blob/master/docs/build_linux.md

- Install Unreal Engine
   a. go to folder where you clone GitHub projects
   b. run commands below:
	git clone -b 4.17 https://github.com/EpicGames/UnrealEngine.git
	cd UnrealEngine
 	git checkout af96417313a908b20621a443175ba91683c238c8
	./Setup.sh
	./GenerateProjectFiles.sh
	make
	
 - Install AirSim
   a. go to folder where you clone GitHub projects
   b. run commands below:
	git clone https://github.com/Microsoft/AirSim.git
	cd AirSim
	./setup.sh
	./build.sh

 - You can now turn start UnrealEngine via navigating to the UnrealEngine folder and running the command below (https://github.com/Microsoft/AirSim/blob/master/docs/unreal_blocks.md)
	Engine/Binaries/Linux/UE4Editor

2. Making an Unreal Project (based on youtube video "Unreal AirSim Setup from scratch)
https://youtu.be/1oY8Qu5maQQ
   a. Navigate to UnrealEngine folder
   b. run the command below
	Engine/Binaries/Linux/UE4Editor
   c. Select "New Project"
   d. Select "C++" tab
   e. Select "Basic Code"
   f. Choose desired quality level and select "No Starter Content" and then select Create Project
	- this will create an Unreal Project in the Unreal Projects folder

3. Setting up a pre-made environment in your new Unreal Project (based on youtube video "Unreal AirSim Setup from scratch)
https://youtu.be/1oY8Qu5maQQ
 - NOTES: This will require you to use Windows OS temporarily, please either use another computer or switch to Windows if your computer is dual booted.
 - USING WINDOWS MACHINE
   - Using the Unreal Market Place download an environment and save to USB drive

 - USING LINUX MACHINE (watch video from 1:06-2:21)
   a. attached USB drive with the pre-made environment, we'll now call it MapProject
   b. from the MapProject, copy the materials from the content folder into the content folder of your new project
   c. copy DerivedDataCache folder to your new project's folder as well as any other folders in the main directory of the MapProject that do not appear in your project's main directory
   d. In the Config folder, merge the code from the MapProject's "DefaultEditor.ini" file and 
      "DefaultEngine.ini" file into your new projects "DefaultEditor.ini" file and 
      "DefaultEngine.ini" file.
	  
4. Setting up Unreal Project to work with AirSim (based on youtube video "Unreal AirSim Setup from scratch" 2:21-4:02)
https://youtu.be/1oY8Qu5maQQ
   a. from AirSim folder, navigate to the Plugins Folder (PATH_TO_AIRSIM_FOLDER/AirSim/Unreal)
   b. copy "Plugins" folder to your project folder.
   c. open up your project's .uproject file
      - can be found in the Unreal Projects folder under your project (PATH_TO_UNREAL Projects/Unreal Projects/YOUR_PROJECT)
   d. add the lines below within the curly braces of the "Modules" section and add a comma at the end of the line before this


			"AdditionalDependencies": [
				"AirSim"
			]
	

   e. add these lines after the "Module" section and put a comma after the closing square bracket of the "Modules" section


	"Plugins": [
		{
			"Name": "AirSim",
			"Enabled": true
		}
	]
   

   f. your .uproject file should look something like the one below after these changes:

	{
		"FileVersion": 3,
		"EngineAssociation": "{1B7E1719-030A-0A14-0036-032A38C22C97}",
		"Category": "",
		"Description": "",
		"Modules": [
			{
				"Name": "DroneFlight",
				"Type": "Runtime",
				"LoadingPhase": "Default",
				"AdditionalDependencies": [
					"AirSim"
				]
			}
		],
		"Plugins": [
			{
				"Name": "AirSim",
				"Enabled": true
			}
		]
	}

   g. If you close the UnrealEngine window with your project on it and re-open it, it should have the pre-made map and plugins necessary to work with AirSim

